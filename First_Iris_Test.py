from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score, classification_report
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# Cargar el conjunto de datos Iris
iris = load_iris()
X, y = iris.data, iris.target

# Análisis Exploratorio de Datos básico
iris_df = pd.DataFrame(X, columns=iris.feature_names)
iris_df['species'] = y

# Visualización de los datos
sns.pairplot(iris_df, hue='species')

# Establecer el estilo y contexto del gráfico
sns.set_style("whitegrid")
sns.set_context("talk")

# Definir una paleta de colores personalizada
palette = sns.color_palette("husl", 3)

# Visualización de los datos con la paleta de colores personalizada
pairplot = sns.pairplot(iris_df, hue='species', palette=palette)
pairplot.fig.suptitle("Iris Dataset Pairplot with Custom Colors", y=1.02)  # Añadir título y ajustar su posición

plt.show()


# Dividir los datos en entrenamiento y prueba
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# Crear el modelo de clasificación
clf = DecisionTreeClassifier()

# Ajuste de hiperparámetros con GridSearchCV
param_grid = {'max_depth': [2, 3, 4, 5, 6, 7, 8],'min_samples_split': [2, 3, 4]}
grid_search = GridSearchCV(clf, param_grid, cv=5)
grid_search.fit(X_train, y_train)

# Mejor modelo después del ajuste de hiperparámetros
best_clf = grid_search.best_estimator_

# Evaluación con validación cruzada
cv_scores = cross_val_score(best_clf, X, y, cv=5)

# Realizar predicciones en el conjunto de prueba
y_pred = best_clf.predict(X_test)

# Evaluación del modelo con múltiples métricas
accuracy = accuracy_score(y_test, y_pred)
report = classification_report(y_test, y_pred)

cv_scores, accuracy, report
