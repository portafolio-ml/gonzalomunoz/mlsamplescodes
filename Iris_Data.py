from sklearn.datasets import load_iris

# Cargar nuevamente el conjunto de datos Iris
iris = load_iris()

# Mostrar la descripción del conjunto de datos Iris
descripcion_iris = iris.DESCR

# Mostrar los nombres de las características (features)
nombres_caracteristicas = iris.feature_names

# Mostrar los nombres de las etiquetas (targets)
nombres_etiquetas = iris.target_names

descripcion_iris, nombres_caracteristicas, nombres_etiquetas

######################################################################
######################################################################

# Imprimir la descripción del conjunto de datos Iris
print("Descripción del Conjunto de Datos Iris:\n")
print(iris.DESCR)

# Imprimir los nombres de las características
print("\nNombres de las Características:")
print(iris.feature_names)

# Imprimir los nombres de las etiquetas
print("\nNombres de las Etiquetas (Clases):")
print(iris.target_names)
